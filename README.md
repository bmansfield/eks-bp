# EKS Boilerplate

A quick EKS boilerplate to get you going quickly.


## About

I'm trying to follow the DRY (Do Not Repeat Yourself) principle. Lets not
reinvent the wheel. Just copy, paste, (clone and reset origin really) and find
and replace necessary parts. Then hit GO (make), and you should have a new EKS
cluster up and running soon.

The cluster which is boilerplated has a few features which are turned on and you
might want to adjust those to your needs. Some of those features are:

* autoscaler
* spot instances
* external dns
* ability to ssh into the nodes
* cloudwatch logging


## Prerequisites

* `eksctl`
* `aws` cli
* an AWS account and ability to access it from your terminal (AWS keys, etc) and
permissions to perform the creation of iam users, roles, etc.
* `jq` for just the removal of the iam user keys


## Things To Change

These are typical things you will want to change for your project specifically

* The cluster config `cluster.yaml`. Just read through it.
  * Names, tags, labels, VPC, CIDR, AZ's, Any particular features you don't want,
  spot instance sizes, prices, additional node groups, etc.
* `Makefile` - I think mostly just the default values such as aws account number,
but you may find some other tweaks
* eks and iam policy json needs you to update the account number
* add your docker registry json file to this directory if you plan on using the
make target for adding the secret feature

These are just "typical" adjustments. I'm sure you can find many more. Again,
this is just a boilerplate as a starting place. Most projects take on a totally
different beast and setup to go with it. It's difficult to make a one size fits
all solution for a boilerplate. So take the advice on typical things to change,
with the grain of salt.


## General Flow

Once you clone this repo, and repoint the origin, and you have all the tools
and prerequisites pointed out earlier and made all of your changes.


### Preface

The `Makefile` has a few global variables you can pass to it or export in your
terminal to override the default values.

```bash
make up CLUSTER=foo
export ACCT_NUMBER=xxxxxxxxxxx make init
```

Just some examples. Read the `Makefile` to understand more.


### Standing Up The Cluster

You could just do

```bash
make all
```

And it should:

* generate new ssh keys
* create all your iam policies
* stand up your cluster
* output your kubernetes cluster context config
* push your private docker registry keys to a secret

If you need to break out your tasks to just do the prep-work, then deploy the
cluster at a later time, or you've already done the prep-work some other way
(i.e. by hand or terraform, etc). Those `make` targets are broken out for you.

```bash
make init
```

Should do the ssh key generation and iam work for you.

If you only want to just stand up the cluster (should also be the default target)

```bash
make up
```


### Clean Up

If you are using this just for some quick prototyping and don't need the cluster
running 24/7, then you can quickly clean up everything in one swipe.

```bash
make clean
```

It should delete the cluster, delete all of the IAM stuff it created, and delete
the ssh keys and cluster config.


### Other

Many of these individual tasks are all broken out into their own target. Just
read the `Makefile` and find the one you want. Or run `make ls` and it should
list them all out and they should be pretty obvious names such as `docker-secret`.


### Post

Now that you have a cluster up and running. You may want to share the access with
others from the team so they can also work and develop on this cluster. See
`eksctl` documentation on how to do this.


## Future Improvements

Some future improvements I would like to make

* Template it further and maybe have a sed function to find and replace easier
* Auto set your kubernetes context after cluster is ready
