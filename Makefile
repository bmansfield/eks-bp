# EKS Makefile
CLUSTER ?= cluster
ACCT_NUMBER ?= 123456789012
IAM_USERNAME ?= deployer
EC2_POLICY_NAME ?= eks-ec2-deploy
CF_POLICY_NAME ?= eks-cf-deploy
EKS_POLICY_NAME ?= eks-eks-deploy
IAM_POLICY_NAME ?= eks-iam-deploy


all: keygen create-iam create-cluster docker-secret

default: create-cluster

init: keygen create-iam

clean cleanup teardown: delete-cluster delete-iam rm-key

ssh-new keygen:
	ssh-keygen \
		-t rsa \
		-b 4096 \
		-C "eks" \
		-f eks
	aws ec2 \
		import-key-pair \
		--key-name "eks" \
		--public-key-material fileb://eks.pub

rm-key:
	aws ec2 \
		delete-key-pair \
		--key-name "eks"

create-iam iam:
	aws iam \
		create-user \
		--user-name $(IAM_USERNAME)
	aws iam \
		create-policy \
		--policy-name assume-role \
		--description "EC2 Policy to assume role for eks deployer" \
		--policy-document file://assume-role-policy.json
	aws iam \
		attach-user-policy \
		--user-name $(IAM_USERNAME) \
		--policy-arn "arn:aws:iam::$(ACCT_NUMBER):policy/assume-deploy"
	aws iam \
		create-role \
		--role-name $(IAM_USERNAME)-role \
		--description "Role for deployment automation" \
		--assume-role-policy-document file://role-trust-policy.json
	aws iam \
		create-policy \
		--policy-name $(EC2_POLICY_NAME) \
		--description "EC2 Policy for eksctl" \
		--policy-document file://ec2-policy.json
	aws iam \
		create-policy \
		--policy-name $(CF_POLICY_NAME) \
		--description "Cloud Formation Policy for eksctl" \
		--policy-document file://cf-policy.json
	aws iam \
		create-policy \
		--policy-name $(EKS_POLICY_NAME) \
		--description "EKS Policy for eksctl" \
		--policy-document file://eks-policy.json
	aws iam \
		create-policy \
		--policy-name $(IAM_POLICY_NAME) \
		--description "IAM Policy for eksctl" \
		--policy-document file://iam-policy.json
	aws iam \
		attach-role-policy \
		--role-name $(IAM_USERNAME)-role \
		--policy-arn arn:aws:iam::$(ACCT_NUMBER):policy/$(EC2_POLICY_NAME)
	aws iam \
		attach-role-policy \
		--role-name $(IAM_USERNAME)-role \
		--policy-arn arn:aws:iam::$(ACCT_NUMBER):policy/$(CF_POLICY_NAME)
	aws iam \
		attach-role-policy \
		--role-name $(IAM_USERNAME)-role \
		--policy-arn arn:aws:iam::$(ACCT_NUMBER):policy/$(EKS_POLICY_NAME)
	aws iam \
		attach-role-policy \
		--role-name $(IAM_USERNAME)-role \
		--policy-arn arn:aws:iam::$(ACCT_NUMBER):policy/$(IAM_POLICY_NAME)
	aws iam \
		create-access-key \
		--user-name $(IAM_USERNAME)

delete-iam rm-iam:
	aws iam \
		delete-role-policy \
		--role-name $(IAM_USERNAME)-role \
		--policy-name $(EC2_POLICY_NAME)
	aws iam \
		delete-role-policy \
		--role-name $(IAM_USERNAME)-role \
		--policy-name $(CF_POLICY_NAME)
	aws iam \
		delete-role-policy \
		--role-name $(IAM_USERNAME)-role \
		--policy-name $(EKS_POLICY_NAME)
	aws iam \
		delete-role-policy \
		--role-name $(IAM_USERNAME)-role \
		--policy-name $(IAM_POLICY_NAME)
	aws iam \
		delete-policy \
		--policy-arn arn:aws:iam::$(ACCT_NUMBER):policy/$(EC2_POLICY_NAME)
	aws iam \
		delete-policy \
		--policy-arn arn:aws:iam::$(ACCT_NUMBER):policy/$(CF_POLICY_NAME)
	aws iam \
		delete-policy \
		--policy-arn arn:aws:iam::$(ACCT_NUMBER):policy/$(EKS_POLICY_NAME)
	aws iam \
		delete-policy \
		--policy-arn arn:aws:iam::$(ACCT_NUMBER):policy/$(IAM_POLICY_NAME)
	aws iam \
		delete-role \
		--role-name $(IAM_USERNAME)-role \
	aws iam \
		delete-user-policy \
		--policy-name assume-role \
	aws iam \
		delete-policy \
		--policy-arn arn:aws:iam::$(ACCT_NUMBER):policy/assume-role
	KEY=$(aws iam \
		list-access-keys \
		--user-name $(IAM_USERNAME) | \
		jq -r '.AccessKeyMetadata[0].AccessKeyId')
	aws iam \
		delete-access-key \
		--access-key-id $(KEY) \
		--user-name $(IAM_USERNAME)
	aws iam \
		delete-user \
		--user-name $(IAM_USERNAME)

create-cluster up:
	eksctl create \
		cluster \
		-f $(CLUSTER).yaml \
		--kubeconfig=./kubeconfig.yml \
		--verbose 5

delete-cluster down:
	eksctl delete \
		cluster \
		--name $(CLUSTER) \
		--verbose 5
	rm ./kubeconfig.yml || true
	rm ./kubeconfig.yml.eksctl.lock || true

docker-secret:
	kubectl create \
		secret \
		generic \
		registrykey \
		--from-file=.dockerconfigjson=dockerregistrycreds.json \
		--type=kubernetes.io/dockerconfigjson \
		--dry-run \
		-o yaml | \
		kubectl apply -f -

ls list:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$'

.PHONY: init create-cluster up delete-cluster down clean cleanup teardown ssh-new keygen rm-key create-iam iam delete-iam rm-iam docker-secret ls list

